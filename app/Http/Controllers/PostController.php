<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Image;
use App\User;
use App\Post;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::join('users','posts.user_id','=','users.id')
            ->select('posts.id','posts.user_id','posts.title','posts.body',
            'users.name as user')->get();

        //$posts = Post::all();
        
        return $posts;
    }

    public function postUser($userId)
    {
        $posts = Post::join('users','posts.user_id','=','users.id')
            ->select('posts.id','posts.user_id','posts.title','posts.body',
            'users.name as user')->where('posts.user_id','=',$userId)->get();

        //$posts = Post::all();
        
        return $posts;
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $post = new Post();
        $post->user_id = $request->user_id;
        $post->title = $request->title;
        $post->body = $request->body;
        $post->slug =  "slug";
        $post->save();
        //return $post;
    }

    
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $post = Post::findOrFail($request->id);
        $post->title = $request->title;
        $post->body = $request->body;
        $post->slug =  str_slug($post->title,"-");
        $post->save();
    }

    public function desactivar(Request $request)
    {
        $post = Post::findOrFail($request->id);
        $post->condicion = '0';
        $post->save();
    }

    public function activar(Request $request)
    {
        $post = Post::findOrFail($request->id);
        $post->condicion = '1';
        $post->save();
    }


    
}
