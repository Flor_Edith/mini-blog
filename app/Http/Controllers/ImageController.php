<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Image;
use App\User;
use App\Post;

class ImageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function imageUser($idUser)
    {
        $user = User::find($idUser);
        $image_user = array();

        foreach ($user->images as $image) {
            array_push($image_user, $image);
        }
        return $image_user;
    }

    public function imagePost($idPost)
    {
        $post = Post::find($idPost);
        $image_post = array();

        foreach ($post->images as $image) {
            array_push($image_post, $image);
        }
        return $image_post;
    }

    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $image = new Image();
        $type = $request->modelo_type;
        $file = $request->url;
        $filename = $file->getClientOriginalName();
        $path='Images/'.$type.'/';
        $file->move($path, $filename);

        $image->modelo_id = $request->modelo_id;
        $image->modelo_type = "App\\".$type;
        $image->url=$path.$filename;
        
        //return $image;
        $image->save();
    }

    
    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $image = Image::findOrFile($request->id);
        $type = $request->modelo_type;
        $file = $request->imagen;
        $filename = $file->getClientOriginalName();
        $path='Images/'.$type.'/';
        $file->move($path, $filename);

        $image->modelo_id = $request->modelo_id;
        $image->modelo_type = 'App\''.$type;
        $image->url=$path.$filename;
        
        $image->save();
    }

}
