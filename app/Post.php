<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Post extends Model
{
    protected $filable = ['title','cuerpo','slug'];
    public function images(){
        return $this->morphMany('App\Image', 'modelo');
    }
    public function user(){
        return $this->belongsTo('App\User');
    }
}
