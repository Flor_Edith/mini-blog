<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $fillable = ['url','modelo_id','modelo_type'];
    public function modelo(){
        return $this->morphTo();
    }
}
