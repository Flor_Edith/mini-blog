<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/post', 'PostController@index');
Route::get('/post/user/{useId}', 'PostController@postUser');
Route::post('/post/registrar','PostController@store');
Route::put('/post/modificar','PostController@update');
Route::put('/post/activar','PostController@activar');
Route::put('/post/desactivar','PostController@update');

Route::post('/image/registrar','ImageController@store');
Route::get('/image/user/{idUser}', 'ImageController@imageUser');
Route::get('/image/post/{idPost}', 'ImageController@imagePost');

