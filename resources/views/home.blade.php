@extends('layouts.app')

@section('content')
<div class="container">
    <template v-if="menu==0">
        <posts></posts>
    </template>
    <template v-if="menu==1">
        <perfil></perfil>
    </template>
    <template v-if="menu==2">
        <foto></foto>
    </template>
    
</div>
@endsection
